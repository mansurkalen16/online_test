<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function test(){
        return $this->hasOne(Test::class, 'id', 'test_id')->first();
    }
    public function submitted(){
        return StudentQuestion::where([
            'student_id' => auth()->user()->id,
            'question_id' => $this->id,
        ])->first();
    }
    public function answers1(){
        return $this->hasOne(Answer::class, 'question_id', 'id')->where([
            'is_correct' => '1'
        ])->first();
    }
    public function answers2(){
        $answers = $this->hasMany(Answer::class, 'question_id', 'id')->where([
            'is_correct' => '1'
        ])->get();
        $arr = [];
        foreach ($answers as $a){
            array_push($arr, $a->id);
        }
        return $arr;
    }
    public function answers(){
        return $this->hasMany(Answer::class, 'question_id', 'id')->get();
    }
    public function answersR(){
        return $this->hasMany(Answer::class, 'question_id', 'id')->inRandomOrder()->get();
    }
}
