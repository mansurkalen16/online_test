<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'middle', 'iin', 'school_id',  'class_id',  'role_code', 'author_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tests(){
        $tests = $this->hasMany(OrderStudent::class, 'student_id','id')->get();
        $arr = [];
        foreach ($tests as $test){
            array_push($arr,$test->test_id);
        }
        return $arr;
    }
    public function school(){
        return $this->hasOne(School::class, 'id', 'school_id')->first();
    }
    public function school_(){
        return $this->belongsTo('School');
    }
}
