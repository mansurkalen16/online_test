<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStudent extends Model
{

    public function test(){
        return $this->hasOne(Test::class, 'id', 'test_id')->first();
    }
    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function student(){
        return $this->hasOne(User::class, 'id', 'student_id')->first();
    }
    public function student_(){
        return $this->belongsTo(User::class);
    }
}
