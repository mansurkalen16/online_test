<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function school(){
        return $this->hasOne(School::class, 'id', 'school_id')->first();
    }
    public function students(){
        return $this->hasMany(User::class, 'class_id', 'id')->where('role_code', '=','student')->get();
    }
}
