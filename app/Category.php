<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    public function subject(){
        return $this->hasOne(Subject::class, 'id', 'subject_id')->first();
    }
    public function parent(){
        return $this->hasOne(Category::class, 'id', 'parent_id')->first();
    }
}
