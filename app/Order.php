<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function test(){
        return $this->hasOne(Test::class, 'id', 'test_id')->first();
    }
    public function school(){
        return $this->hasOne(School::class, 'id', 'school_id')->first();
    }
    public function bought_by(){
        return $this->hasOne(User::class, 'id', 'bought_by')->first();
    }
    public function order_students(){
        return $this->hasMany(OrderStudent::class);
    }
}
