<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id')->first();
    }
    public function order_for_me(){
        return $this->hasMany(OrderStudent::class, 'test_id', 'id')->where('student_id', auth()->user()->id)->first();
    }
    public function questions(){
        return $this->hasMany(Question::class, 'test_id', 'id')->get();
    }
    public function questionsR(){
        return $this->hasMany(Question::class, 'test_id', 'id')->inRandomOrder()->get();
    }
}
