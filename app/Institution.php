<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    public function schools(){
        return $this->hasMany(Group::class, 'institution_id', 'id')->get();
    }
}
