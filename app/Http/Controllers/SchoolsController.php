<?php

namespace App\Http\Controllers;

use App\Center;
use App\Imports\SchoolsImport;
use App\Institution;
use App\School;
use Illuminate\Http\Request;
use Excel;

class SchoolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = school::latest()->paginate(10);
        $centers = Center::get();
        $institutions = Institution::get();

        return view('school.index',[
            'centers' => $centers,
            'institutions' => $institutions,
            'schools' => $schools
        ])
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centers = Center::get();
        $institutions = Institution::get();
        return view('school.create', [
            'centers' => $centers,
            'institutions' => $institutions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'center_id' => ['required', 'string', 'max:255'],
            'institution_id' => ['required', 'string', 'max:255'],
            'language_id' => ['required', 'string', 'max:255'],
        ]);
        $school = new school();
        $school->name = $request->name;
        $school->institution_id = $request->institution_id;
        $school->center_id = $request->center_id;
        $school->address = $request->address;
        $school->phone = $request->phone;
        $school->language_id = $request->language_id;
        $school->director_id = 1;
        $school->school_type_id = 1;
        $school->kato = 1;
        $school->author_id = auth()->user()->id;
        $school->save();

        return redirect()->route('schools')
            ->with('success','school created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centers = Center::get();
        $school = school::find($id);
        $institutions = Institution::get();
        return view('school.update', [
            'school' => $school,
            'centers' => $centers,
            'institutions' => $institutions
        ]);
    }
    public function import(Request $request)
    {
        $request->validate([
            'file' => ['required', 'mimes:xls,xlsx'],
        ]);
        $path1 = $request->file('file')->store('temp');
        $path=storage_path('app').'/'.$path1;

        Excel::import(new SchoolsImport($request->center_id, $request->language_id, $request->institution_id), $path);

        return redirect()->back()
            ->with('success','Schools imported successfully.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'institution_id' => ['required', 'string', 'max:255'],
            'center_id' => ['required', 'string', 'max:255'],
            'language_id' => ['required', 'string', 'max:255'],
        ]);
        $school = school::find($id);
        $school->name = $request->name;
        $school->center_id = $request->center_id;
        $school->institution_id = $request->institution_id;
        $school->address = $request->address;
        $school->phone = $request->phone;
        $school->language_id = $request->language_id;
        $school->director_id = 1;
        $school->school_type_id = 1;
        $school->kato = 1;
        $school->author_id = auth()->user()->id;
        $school->update();

        return redirect()->route('schools')
            ->with('success','school updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = school::find($id);
        $school->delete();

        return redirect()->route('schools')
            ->with('success','school deleted successfully');
    }
}
