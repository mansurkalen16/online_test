<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $question = Question::find($id);
        return view('answer.create', [
            'question' => $question
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->question_id as $key => $q_id){
            $answer = new Answer();
            $answer->answer = $request->answer[$key];
            $answer->question_id = $request->question_id[$key];
            $answer->is_correct = in_array($key+1,$request->is_correct)?1:0;
            $answer->author_id = auth()->user()->id;
            $answer->save();
        }

        return redirect()->route('tests')
            ->with('success','answers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = question::find($id);
        return view('answer.update', [
            'question' => $question,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $answers = Answer::where('question_id',$request->question_id[0])->delete();
        foreach ($request->question_id as $key => $q_id){
            $answer = new Answer();
            $answer->answer = $request->answer[$key];
            $answer->question_id = $request->question_id[$key];
            $answer->is_correct = in_array($key+1,$request->is_correct)?1:0;
            $answer->author_id = auth()->user()->id;
            $answer->save();
        }

        return redirect()->route('tests')
            ->with('success','answers created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        //
    }
}
