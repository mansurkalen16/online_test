<?php

namespace App\Http\Controllers;

use App\StudentQuestion;
use Illuminate\Http\Request;

class StudentQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentQuestion  $studentQuestion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sq = StudentQuestion::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentQuestion  $studentQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentQuestion $studentQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentQuestion  $studentQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentQuestion $studentQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentQuestion  $studentQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentQuestion $studentQuestion)
    {
        //
    }
}
