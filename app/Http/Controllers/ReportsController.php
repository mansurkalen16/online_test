<?php

namespace App\Http\Controllers;

use App\Center;
use App\Group;
use App\OrderStudent;
use App\School;
use App\Test;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function all(){
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('tests','tests.id','=','order_students.test_id')
            ->join('schools','users.school_id','=','schools.id')
            ->join('centers','schools.center_id','=','centers.id')
            ->select('tests.name','tests.id', DB::raw("count(distinct centers.id) as count"),
                DB::raw("avg(order_students.total) as avg"))
            ->where('order_students.status_id','=','5')->groupBy('tests.name','tests.id')->get();
        return view('reports.all', [
            'results' => $results
        ]);
    }
    public function all_(){
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('tests','tests.id','=','order_students.test_id')
            ->select('tests.name','tests.id',
                DB::raw("avg(order_students.total) as avg"))->where('users.school_id',auth()->user()->school_id)
            ->where('order_students.status_id','=','5')->groupBy('tests.name','tests.id')->get();
        return view('reports.all_', [
            'results' => $results
        ]);
    }
    public function index($tid){
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('schools','users.school_id','=','schools.id')
            ->join('centers','schools.center_id','=','centers.id')
            ->select('centers.name','centers.id','centers.address','centers.phone', DB::raw("count(distinct schools.id) as count"),
                DB::raw("avg(order_students.total) as avg"))
            ->where('order_students.test_id', '=', $tid)
            ->where('order_students.status_id','=','5')->groupBy('centers.id','centers.name','centers.address','centers.phone')->get();
        return view('reports.index', [
            'results' => $results,
            'tid' => $tid
        ]);
    }
    public function center($id, $tid){
        $center = Center::find($id);
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('schools','users.school_id','=','schools.id')
            ->select('schools.name','schools.id','schools.address','schools.phone', DB::raw("count(distinct users.id) as s_count"),
                DB::raw("count(distinct users.class_id) as g_count"),
                DB::raw("avg(order_students.total) as avg"))->where('schools.center_id', $id)
            ->where('order_students.test_id', '=', $tid)
            ->where('order_students.status_id','=','5')
            ->groupBy('schools.id','schools.name','schools.address','schools.phone')->get();
        return view('reports.center', [
            'results' => $results,
            'center' => $center,
            'tid' => $tid
        ]);
    }
    public function school($id, $tid){
        $school = School::find($id);
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('groups','users.class_id','=','groups.id')
            ->select('groups.id','groups.name', DB::raw("count(distinct users.id) as count"),
                DB::raw("avg(order_students.total) as avg"))->where('groups.school_id', $id)
            ->where('order_students.test_id', '=', $tid)
            ->where('order_students.status_id','=','5')
            ->groupBy('groups.id','groups.name')->get();
        return view('reports.school', [
            'results' => $results,
            'school' => $school,
            'tid' => $tid
        ]);
    }
    public function group($id, $tid){
        $group = Group::find($id);
        $results = DB::table('order_students')->join('users','users.id','=','order_students.student_id')
            ->join('groups','users.class_id','=','groups.id')
            ->select('users.id','users.name','users.middle','users.surname','users.iin', 'order_students.total')
            ->where('users.class_id', $id)
            ->where('order_students.status_id','=','5')
            ->where('order_students.test_id', '=', $tid)
            ->groupBy('users.id','users.name','users.middle','users.surname','users.iin', 'order_students.total')->get();
        return view('reports.group', [
            'results' => $results,
            'group' => $group,
            'tid' => $tid
        ]);
    }
    public function student($id, $tid){
        $user = User::find($id);
        $order = OrderStudent::where([
            'student_id' => $id,
            'test_id' => $tid,
        ])->first();
        $test = Test::find($tid);
        return view('reports.student', [
            'test' => $test,
            'order' => $order,
            'user' => $user
        ]);
    }
}
