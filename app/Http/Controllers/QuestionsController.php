<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Category;
use App\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $question = Question::find();
//        return view('question.index', [
//            'question' => $question
//        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $category = Category::find($id);
        return view('question.create', [
            'category' => $category,
            'test_id' => $id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => ['required', 'string', 'max:255'],
            'level' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'test_id' => ['required', 'string', 'max:255'],
            'answer_count' => ['required', 'string', 'max:255'],
        ]);
        $question = new Question();
        $question->question = $request->question;
        $question->level = $request->level;
        $question->type = $request->type;
        $question->test_id = $request->test_id;
        $question->answer_count = $request->answer_count;
        $question->author_id = auth()->user()->id;

        if($question->save()){
            return redirect()->route('answers.create', $question->id)
                ->with('success','Question created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        return view('question.show', [
            'question' => $question
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        return view('question.update', [
            'question' => $question,
            'test_id' => $question->test_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => ['required', 'string', 'max:255'],
            'level' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'test_id' => ['required', 'string', 'max:255'],
            'answer_count' => ['required', 'string', 'max:255'],
        ]);
        $question = Question::find($id);
        $question->question = $request->question;
        $question->level = $request->level;
        $question->answer_count = $request->answer_count;
        $question->type = $request->type;
        $question->test_id = $request->test_id;
        $question->author_id = auth()->user()->id;

        if($question->save()){
            return redirect()->route('answers.edit', $question->id)
                ->with('success','Question updated successfully.');
        }
    }
    public function copy(Request $request)
    {
        $question_o = Question::find($request->question_id);
        if($question_o){
            $question = new Question();
            $question->question = $question_o->question;
            $question->level = $question_o->level;
            $question->answer_count = $question_o->answer_count;
            $question->type = $question_o->type;
            $question->test_id = $request->test_id;
            $question->author_id = auth()->user()->id;
            if($question->save()){
                foreach ($question_o->answers() as $answer_o){
                    $answer = new Answer();
                    $answer->answer = $answer_o->answer;
                    $answer->question_id = $question->id;
                    $answer->is_correct = $answer_o->is_correct;
                    $answer->author_id = auth()->user()->id;
                    $answer->save();
                }
                return redirect()->back()
                    ->with('success','Question copied successfully.');
        }
        }else{
            return redirect()->back()
                ->with('error','Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect()->back()
            ->with('success','Question deleted successfully.');
    }
}
