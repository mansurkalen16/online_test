<?php

namespace App\Http\Controllers;

use App\Group;
use App\Imports\UsersImport;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Excel;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate(5);

        return view('user.index',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role_code' => ['required', 'string', 'max:255'],
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->middle = $request->middle;
        $user->email = $request->email;
        $user->password = Hash::make('123');
        $user->role_code = $request->role_code;
        $user->author_id = auth()->user()->id;
        $user->save();

        return redirect()->route('users')
            ->with('success','User created successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $request->validate([
            'file' => ['required', 'mimes:xls,xlsx'],
        ]);
        $path1 = $request->file('file')->store('temp');
        $path=storage_path('app').'/'.$path1;

        $group = Group::find($request->class_id);
        Excel::import(new UsersImport($group->id, $group->school_id), $path);

        return redirect()->back()
            ->with('success','Students imported successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user  = User::find($id);
        return view('user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user  = User::find($id);
        return view('user.update',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role_code' => ['required', 'string', 'max:255'],
        ]);
        $user  = User::find($id);
        $user->update([
            $user->name = $request->name,
        $user->surname = $request->surname,
        $user->middle = $request->middle,
        $user->email = $request->email,
        $user->password = Hash::make('123'),
        $user->role_code = $request->role_code,
        $user->author_id = auth()->user()->id
        ]);

        return redirect()->route('users')
            ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->route('users')
            ->with('success','User deleted successfully');
    }
}
