<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subject;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Categories = Category::latest()->paginate(10);

        return view('category.index',compact('Categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::get();
        $categories = Category::where('parent_id',0)->get();
        return view('category.create', [
            'subjects' => $subjects,
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_kz' => ['required', 'string', 'max:255'],
            'name_ru' => ['required', 'string', 'max:255'],
            'subject_id' => ['required', 'string', 'max:255'],
        ]);
        $Category = new Category();
        $Category->name_kz = $request->name_kz;
        $Category->name_ru = $request->name_ru;
        $Category->subject_id = $request->subject_id;
        $Category->parent_id = $request->parent_id;
        $Category->author_id = auth()->user()->id;
        $Category->save();

        return redirect()->route('Categories')
            ->with('success','Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('Category.show',compact('Category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('parent_id',0)->where('id','!=', $id)->get();
        $subjects = Subject::get();
        $Category = Category::find($id);
        return view('category.update', [
            'Category' => $Category,
            'subjects' => $subjects,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_kz' => 'required',
            'name_ru' => ['required', 'string', 'max:255'],
        ]);
        $Category = Category::find($id);
        $Category->name_kz = $request->name_kz;
        $Category->name_ru = $request->name_ru;
        $Category->subject_id = $request->subject_id;
        $Category->parent_id = $request->parent_id;
        $Category->author_id = auth()->user()->id;
        $Category->update();

        return redirect()->route('Categories')
            ->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Category = Category::find($id);
        $Category->delete();

        return redirect()->route('Categories')
            ->with('success','Category deleted successfully');
    }
}
