<?php

namespace App\Http\Controllers;

use App\Category;
use App\Group;
use App\Order;
use App\OrderStudent;
use App\School;
use App\StudentQuestion;
use App\Test;
use App\User;
use Illuminate\Http\Request;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = test::latest()->paginate(10);
        if (auth()->user()->role_code == 'student'){
            $tests = Test::whereIn('id', auth()->user()->tests())->paginate(10);
        }
        return view('test.index',compact('tests'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('test.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'language_id' => ['required', 'string', 'max:255'],
        ]);
        $test = new test();
        $test->name = $request->name;
        $test->language_id = $request->language_id;
        $test->category_id = $request->category_id;
        $test->status_id = 1;
        $test->author_id = auth()->user()->id;
        $test->save();

        return redirect()->route('tests')
            ->with('success','test created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test = Test::find($id);
        $tests = Test::where('id','!=', $id)->get();
        return view('test.show', [
            'test' => $test,
            'tests' => $tests
        ]);
    }
    public function start($id)
    {
        $test = Test::find($id);
        return view('test.start', [
            'test' => $test
        ]);
    }

    public function result($id)
    {
        $user = User::find(auth()->user()->id);
        $order = OrderStudent::where([
            'student_id' => $user->id,
            'test_id' => $id,
        ])->first();
        $test = Test::find($id);
        return view('test.result', [
            'test' => $test,
            'order' => $order,
            'user' => $user
        ]);
    }

    public function buy($id)
    {
        $test = Test::find($id);
        $schools = School::get();
        $groups = Group::where('school_id', auth()->user()->id)->get();
        return view('test.buy', [
            'test' => $test,
            'schools' => $schools,
            'groups' => $groups
        ]);
    }

    public function buy_(Request $request)
    {
        $order = new Order();
        $order->test_id = $request->test_id;
        $order->school_id = $request->school_id;
        $order->bought_by = auth()->user()->id;
        $order->operator_id = 0;
        $order->buy_type_id = 2;
        $order->status = 1;
        if ($order->save()){
            foreach ($request->student_id as $student){
                $os = new OrderStudent();
                $os->order_id = $order->id;
                $os->test_id = $request->test_id;
                $os->student_id = $student;
                $os->status_id = 1;
                $os->total = 0;
                $os->save();
            }
            return redirect()->route('tests')
                ->with('success','successfully.');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::get();
        $test = test::find($id);
        return view('test.update', [
            'test' => $test,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $test = test::find($id);
        $test->name = $request->name;
        $test->language_id = $request->language_id;
        $test->category_id = $request->category_id;
        $test->status_id = 1;
        $test->author_id = auth()->user()->id;
        $test->update();

        return redirect()->route('tests')
            ->with('success','test updated successfully');
    }
    public function answer(Request $request)
    {
        $question = StudentQuestion::where([
            'student_id' => auth()->user()->id,
            'question_id' => $request->q,
        ])->first();
        if(!$question){
            $question = new StudentQuestion();
        }
        $question->student_id = auth()->user()->id;
        $question->question_id = $request->q;
        $question->checked = $request->a;
        $question->is_correct = 0;
        $question->is_submitted = 0;
        $question->point = 0;
        $question->save();
        echo '1';
    }
    public function submit($id)
    {
        $order = OrderStudent::where([
            'student_id' => auth()->user()->id,
            'test_id' => $id,
        ])->first();
        if ($order){
            $total = 0;
            $test = Test::find($id);
            foreach ($test->questions() as $q){
                $question = StudentQuestion::where([
                    'student_id' => auth()->user()->id,
                    'question_id' => $q->id,
                ])->first();
                if($question){
                    if($q->type == '1' && $q->answers1()->id == $question->checked){
                        $question->is_correct = 1;
                        $question->is_submitted = 1;
                        $question->point = 1;

                        $total = $total + 1;
                    }
                    if($q->type == '2'){
                        $ex = explode(',',$question->checked);
                        $c = 0;
                        foreach ($q->answers2() as $i) {
                            if (in_array($i,$ex)) $c++;
                        }
                        $question->is_correct = $c/count($q->answers2())>0?1:0;
                        $question->is_submitted = 1;
                        $question->point = $c/count($q->answers2());

                        $total = $total + $c/count($q->answers2());
                    }
                    $question->save();
                }
            }
            $order->total = $total;
            $order->status_id = 5;
            $order->save();

            return redirect()->route('tests.result', $id)
                ->with('success','Finished');
        }
        return redirect()->back()
            ->with('error','something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = test::find($id);
        $test->delete();

        return redirect()->route('tests')
            ->with('success','test deleted successfully');
    }
}
