<?php

namespace App\Http\Controllers;

use App\Institution;
use Illuminate\Http\Request;

class InstitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutions = Institution::latest()->paginate(10);

        return view('institution.index',compact('institutions'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institution.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_kz' => ['required', 'string', 'max:255'],
            'name_ru' => ['required', 'string', 'max:255'],
        ]);
        $institution = new institution();
        $institution->code = '1';
        $institution->name_kz = $request->name_kz;
        $institution->name_ru = $request->name_ru;
//        $institution->author_id = auth()->user()->id;
        $institution->save();

        return redirect()->route('institutions')
            ->with('success','institution created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institution = institution::find($id);
        return view('institution.update',compact('institution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_kz' => ['required', 'string', 'max:255'],
            'name_ru' => ['required', 'string', 'max:255'],
        ]);
        $institution = institution::find($id);
        $institution->code = "1";
        $institution->name_kz = $request->name_kz;
        $institution->name_ru = $request->name_ru;
//        $institution->author_id = auth()->user()->id;
        $institution->update();

        return redirect()->route('institutions')
            ->with('success','institution updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institution = institution::find($id);
        $institution->delete();

        return redirect()->route('institutions')
            ->with('success','institution deleted successfully');
    }
}
