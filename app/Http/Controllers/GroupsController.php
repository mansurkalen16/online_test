<?php

namespace App\Http\Controllers;

use App\School;
use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = group::latest()->paginate(10);

        return view('group.index',compact('groups'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schools = School::get();
        return view('group.create', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'school_id' => ['required', 'string', 'max:255'],
        ]);
        $group = new group();
        $group->name = $request->name;
        $group->school_id = $request->school_id;
        $group->save();

        return redirect()->route('groups')
            ->with('success','group created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('group.show',compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schools = School::get();
        $group = group::find($id);
        return view('group.update', [
            'group' => $group,
            'schools' => $schools
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $group = group::find($id);
        $group->name = $request->name;
        $group->school_id = $request->school_id;
        $group->update();

        return redirect()->route('groups')
            ->with('success','group updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = group::find($id);
        $group->delete();

        return redirect()->route('groups')
            ->with('success','group deleted successfully');
    }

    public function students(Request $request){
        $group = Group::find($request->id);
        return view('group.students',compact('group'));
    }
    public function add_student($id){
        return view('group.add_student',compact('id'));
    }
    public function save_student(Request $request){

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
        ]);
        $group = Group::find($request->group_id);
        $user = new User();
        $user->iin = $request->iin;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->middle = $request->middle;
        $user->email = $request->email;
        $user->school_id = $group->school_id;
        $user->class_id = $request->group_id;
        $user->password = Hash::make('123');
        $user->role_code = "student";
        $user->author_id = auth()->user()->id;
        $user->save();
        return redirect()->route('groups')
            ->with('success','Student added');
    }
}
