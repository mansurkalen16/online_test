<?php

namespace App\Http\Controllers;

use App\Center;
use Illuminate\Http\Request;

class CentersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centers = center::latest()->paginate(10);

        return view('center.index',compact('centers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('center.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ]);
        $center = new center();
        $center->kato = 1;
        $center->name = $request->name;
        $center->address = $request->address;
        $center->phone = $request->phone;
//        $center->author_id = auth()->user()->id;
        $center->save();

        return redirect()->route('centers')
            ->with('success','center created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
        return view('center.show',compact('center'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $center = center::find($id);
        return view('center.update',compact('center'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
        ]);
        $center = center::find($id);
        $center->kato = 1;
        $center->name = $request->name;
        $center->address = $request->address;
        $center->phone = $request->phone;
//        $center->author_id = auth()->user()->id;
        $center->update();

        return redirect()->route('centers')
            ->with('success','center updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $center = center::find($id);
        $center->delete();

        return redirect()->route('centers')
            ->with('success','center deleted successfully');
    }
}
