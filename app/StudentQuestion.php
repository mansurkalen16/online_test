<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentQuestion extends Model
{
    protected $table = "students_questions";
}
