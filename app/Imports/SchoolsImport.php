<?php

namespace App\Imports;

use App\School;
use Maatwebsite\Excel\Concerns\ToModel;

class SchoolsImport implements ToModel
{
    protected $_i = 0;
    protected $_l = 0;
    protected $_c = 0;

    public function  __construct($c,$l,$i) {
        $this->_i = $i;
        $this->_c = $c;
        $this->_l = $l;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new School([
            'name' => $row[0],
            'address' => $row[1],
            'phone' => $row[2],
            'kato' => 1,
            'school_type_id' => 1,
            'center_id' => $this->_c,
            'language_id' => $this->_l,
            'institution_id' => $this->_i,
            'director_id' => 0,
            'author_id' => 0,
        ]);
    }
}
