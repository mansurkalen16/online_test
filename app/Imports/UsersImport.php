<?php

namespace App\Imports;

use App\Group;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    protected $_id = 0;
    protected $_s = 0;

    public function  __construct($id,$s) {
        $this->_id = $id;
        $this->_s = $s;
    }
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $id = $this->_id;
        $s = $this->_s;
        $student = User::where('iin', $row[0])->orWhere('email', $row[4])->first();
        if(!$student){
            return new User([
                'iin' => $row[0],
                'name' => $row[1],
                'middle' => $row[2],
                'surname' => $row[3],
                'email' => $row[4],
                'school_id' => $s,
                'class_id' => $id,
                'password' => Hash::make('123'),
                'role_code' => 'student',
                'author_id' => 0,
            ]);
        }
    }
}
