<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name',
        'address',
        'phone',
        'kato',
        'school_type_id',
        'center_id',
        'language_id' ,
        'institution_id',
        'director_id' ,
        'author_id' ];
    public function center_(){
        return $this->belongsTo('Center');
    }
    public function center(){
        return $this->hasOne(Center::class, 'id', 'center_id')->first();
    }
    public function institution(){
        return $this->hasOne(Institution::class, 'id', 'institution_id')->first();
    }
    public function groups(){
        return $this->hasMany(Group::class, 'school_id', 'id')->get();
    }
}
