<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'home' => 'Home',
    'users' => 'Users',
    'centers' => 'Centers',
    'reports' => 'Reports',
    'orders' => 'Orders',
    'references' => 'References',
    'institutions' => 'Institutions',
    'institution' => 'Institution',
    'schools' => 'Schools',
    'classes' => 'Classes',
    'tests' => 'Tests',
    'subjects' => 'Subjects',
    'topics' => 'Topics',
    'login' => 'Log in',
    'registration' => 'Registration',
    'logout' => 'Logout',
    'you have successfully logged in' => 'You have successfully logged in!',
    'a list of users' => 'A list of users',
    'Add user' => 'Add user',
    'name' => 'Name',
    'surname' => 'Surname',
    'middle name' => 'Middle name',
    'role' => 'role',
    'actions' => 'Actions',
    'email' => 'Email',
    'Add new user' => 'Add new user',
    'Back' => 'Back',
    'Save' => 'Save',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Change user data' => 'Change user data',
    'Regional Testing Centers' => 'Regional Testing Centers',
    'add rtc' => 'Add RTC',
    'name_' => 'Name',
    'address' => 'Address',
    'phone' => 'Phone',
    'rtc' => 'RTC',
    'Data change' => 'Data change',
    'test name' => 'Test name',
    'The amount of RTC' => 'The amount of RTC',
    'Grade point average' => 'Grade point average',
    'Number of schools' => 'Number of schools',
    'school' => 'School',
    'Number of classes' => 'Number of classes',
    'Number of students' => 'Number of students',
    'RTC Report' => 'RTC Report',
    'School Report' => 'School Report',
    'class' => 'Class',
    'Class report' => 'Class report',
    'FULL NAME' => 'FULL NAME',
    'IIN' => 'IIN',
    'Grade' => 'Grade',
    'Handed over' => 'Handed over',
    'test' => 'Test',
    'result' => 'Result',
    'Right answers' => 'Right answers',
    'answers' => 'Answers',
    'Test name' => 'Test name',
    'bought by' => 'Bought by',
    'create new order' => 'Create new order',
    'Number of questions' => 'Number of questions',
    'Buy test' => 'Buy test',
    'Buy' => 'Buy',
    'successfully' => 'Successfully',
    'Name in Kazakh' => 'Name in Kazakh',
    'Name in Russian' => 'Name in Russian',
    'Add new institution' => 'Add new institution',
    'type' => 'Type',
    'import from excel' => 'import from excel',
    'add school' => 'Add school',
    'Language' => 'Language',
    'Change school details' => 'Change school details',
    'Students_' => 'Students',
    'List' => 'List',
    'add student' => 'Add student',
    'add new class' => 'Add new class',
    'add subject' => 'Add subject',
    'subject' => 'Subject',
    'add new topic' => 'Add new topic',
    'parent topic' => 'Parent topic',
    'change topic' => 'Change topic',
    'topic' => 'Topic',
    'close' => 'Close',
    'import' => 'Import',
    'import excel file' => 'Import excel file',
    'add test' => 'Add test',
    'add question' => 'Add question',
    'copy question' => 'Copy question',
    'change question' => 'Change question',
    'question' => 'Question',
    'number of answer' => 'Number of Answers',
    'number of correct answer' => 'Number of correct answers',
    'level' => 'Level',
    'change answers of question' => 'Change answers of question',
    'answer' => 'Answer',
    'correct' => 'Correct',
    'start' => 'Start',
    'finish' => 'Finish',
    'add answer' => 'Add answer',

];
