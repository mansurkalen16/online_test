@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.topics')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('Categories.create') }}">@lang('app.add new topic')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>№</th>
                        <th>@lang('app.Name in Kazakh')</th>
                        <th>@lang('app.Name in Russian')</th>
                        <th>@lang('app.subject')</th>
                        <th>@lang('app.parent topic')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($Categories as $Category)
                        <tr>
                            <td>{{ $Category->id }}</td>
                            <td>{{ $Category->name_kz }}</td>
                            <td>{{ $Category->name_ru }}</td>
                            <td>{{ $Category->subject()->name_kz }}</td>
                            <td>{{ $Category->parent()?$Category->parent()->name_kz:'Главная тема' }}</td>
                            <td>
                                <form action="{{ route('Categories.destroy',$Category->id) }}" method="POST">


                                    <a class="btn btn-info btn-sm" href="{{ route('Categories.edit',$Category->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $Categories->links() !!}
            </div>
        </div>
    </div>
@endsection
