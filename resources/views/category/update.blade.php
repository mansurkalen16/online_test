@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.change topic') {{$Category->name_kz}}  </h2>
                </div>

                <div class="pull-right">

                </div>
                @php $locale = session()->get('locale'); @endphp
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('Categories.update', $Category->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Name in Kazakh'):</strong>
                                <input type="text" name="name_kz" value="{{$Category->name_kz}}" class="form-control" placeholder="@lang('app.Name in Kazakh')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Name in Russian'):</strong>
                                <input type="text" name="name_ru"  value="{{$Category->name_ru}}" class="form-control" placeholder="@lang('app.Name in Russian')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.subject'):</strong>
                                <select name="subject_id" class="form-control">
                                    @foreach($subjects as $subject)
                                        <option value="{{$subject->id}}" @if($subject->id == $Category->subject_id) selected @endif>
                                            @if($locale == 'kz')
                                                {{$subject->name_kz}}
                                            @elseif($locale == 'ru')
                                                {{$subject->name_ru}}
                                            @elseif($locale == 'en')
                                                {{$subject->name_kz}} /
                                        {{$subject->name_ru}}
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.parent topic'):</strong>
                                <select name="parent_id" class="form-control">
                                    <option value="0" @if(0 == $Category->subject_id) selected @endif>@lang('app.parent topic')</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($category->id == $Category->parent_id) selected @endif>
                                            @if($locale == 'kz')
                                                {{$category->name_kz}}
                                            @elseif($locale == 'ru')
                                                {{$category->name_ru}}
                                            @elseif($locale == 'en')
                                                {{$category->name_kz}} /
                                                {{$category->name_ru}}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                           <a class="btn btn-info btn-sm" href="{{ route('Categories') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
