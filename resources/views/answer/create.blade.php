@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/ckeditor.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.add answer')</h2>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="{{route('answers.store')}}" method = "post">

                        @csrf

                        <div class="row">
                        @for($i=0;$i<$question->answer_count;$i++)
                            <div class="col-xs- col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>@lang('app.answer'):</strong>
                                    <br>
                                    @lang('app.correct'):
                                    @if($question->type == 1)
                                        <input type="radio" name="is_correct[]" value="{{$i+1}}" >
                                    @else
                                        <input type="checkbox" name="is_correct[]" value="{{$i+1}}">
                                    @endif
                                    <textarea name="answer[]" class="form-control"></textarea>

                                    <input type="hidden" name="question_id[]" value="{{$question->id}}" >
                                </div>
                            </div>
                        @endfor
                            <div class="col-xs-12 col-sm-12 col-md-12">
                        <input type="submit" class="btn btn-primary"  value="@lang('app.Save')">
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>

    <script>
        function send() {
            const data =
        }
    </script>
@endsection
