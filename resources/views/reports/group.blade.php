@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h4>@lang('app.Class report') {{$group->name}}</h4><br>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>@lang('app.FULL NAME')</th>
                        <th>@lang('app.IIN')</th>
                        <th>@lang('app.Grade')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($results as $result)
                        <tr>
                            <td>{{$result->id}} {{ $result->surname }} {{ $result->name }} {{ $result->middle }}</td>
                            <td>{{ $result->iin }}</td>
                            <td>{{ $result->total }}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{ route('reports.student',['id' => $result->id, 'tid' => $tid]) }}">@lang('app.More')</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
