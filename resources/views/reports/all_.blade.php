@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h4>Отчет по школу {{auth()->user()->school()->name}}</h4><br>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>@lang('app.test name')</th>
                        <th>@lang('app.The amount of RTC')</th>
                        <th>@lang('app.Grade point average')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($results as $result)
                        <tr>
                            <td>{{ $result->name }}</td>
                            <td>{{ $result->avg }}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{ route('reports.school',['id' => auth()->user()->school_id, 'tid' => $result->id]) }}">@lang('app.More')</a>
{{--                                <a class="btn btn-info btn-sm" href="{{ route('reports', $result->id) }}">Подробнее</a>--}}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
