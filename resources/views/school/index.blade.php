@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.schools')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('schools.create') }}">@lang('app.add school')</a>
                            <a class="btn btn-info btn-sm text-white" onclick="import_()" >@lang('app.import from excel')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.name_')</th>
                        <th>@lang('app.rtc')</th>
                        <th>@lang('app.type')</th>
                        <th>@lang('app.address')</th>
                        <th>@lang('app.phone')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($schools as $school)
                        <tr>
                            <td>{{ $school->id }}</td>
                            <td>{{ $school->name }}</td>
                            <td>{{ $school->center()->name }}</td>
                            <td>{{ isset($school->institution()->name_kz)>0?$school->institution()->name_kz:"" }}</td>
                            <td>{{ $school->address }}</td>
                            <td>{{ $school->phone }}</td>
                            <td>
                                <form action="{{ route('schools.destroy',$school->id) }}" method="POST">


                                    <a class="btn btn-info btn-sm" href="{{ route('schools.edit',$school->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-danger">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $schools->links() !!}
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="import_" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('app.import from excel')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('schools.import')}}" method = "post" enctype="multipart/form-data" >
                    @csrf
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>@lang('app.Language'):</strong>
                            <select name="language_id" class="form-control">
                                <option value="1">Қазақша</option>
                                <option value="2">Русский</option>
                                <option value="3">English</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>@lang('app.rtc'):</strong>
                            <select name="center_id" class="form-control">
                                @foreach($centers as $center)
                                    <option value="{{$center->id}}">{{$center->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>@lang('app.type'):</strong>
                            <select name="institution_id" class="form-control">
                                @foreach($institutions as $institution)
                                    <option value="{{$institution->id}}">{{$institution->name_kz}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-body">
                        <b>@lang('app.import excel file')</b>
                        <input type="file" name="file" class="form-control" >
                    </div>
                    <input type="hidden" id="class_id" name="class_id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">@lang('app.close')</button>
                        <button type="submit" class="btn btn-info btn-sm" >@lang('app.import')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function import_(id) {
            $('#import_').modal('show')
            document.getElementById("class_id").value = id;
        }
    </script>
@endsection
