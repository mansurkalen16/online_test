@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.add school')</h2>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('schools.store')}}" method = "post">

                    @csrf

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.name_'):</strong>
                                <input type="text" name="name" class="form-control" placeholder="@lang('app.name_')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Language'):</strong>
                                <select name="language_id" class="form-control">
                                    <option value="1">Қазақша</option>
                                    <option value="2">Русский</option>
                                    <option value="3">English</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.rtc'):</strong>
                                <select name="center_id" class="form-control">
                                    @foreach($centers as $center)
                                        <option value="{{$center->id}}">{{$center->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.type'):</strong>
                                <select name="institution_id" class="form-control">
                                    @foreach($institutions as $institution)
                                        <option value="{{$institution->id}}">{{$institution->name_kz}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.address'):</strong>
                                <textarea name="address" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.phone'):</strong>
                                <textarea name="phone" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                           <a class="btn btn-info btn-sm" href="{{ route('schools') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
