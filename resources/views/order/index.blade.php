@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.orders')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('tests') }}">@lang('app.create new order')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.test name')</th>
                        <th>@lang('app.school')</th>
                        <th>@lang('app.bought by')</th>
                    </tr>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->test()->name }}</td>
                            <td>{{ $order->school()->name }}</td>
                            <td>{{ $order->bought_by()->surname }} {{ $order->bought_by()->name }}</td>
                        </tr>
                    @endforeach
                </table>

                {!! $orders->links() !!}
            </div>
        </div>
    </div>
@endsection
