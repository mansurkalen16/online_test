<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'USTUDY') }}</title>

    <!-- Scripts -->
<!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="{{ asset('js/jquery.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.js') }}" defer></script>
    <script src="{{ asset('js/adminlte.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
<!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('favicon.ico')}} " type="image/x-icon">
</head>
<body>
<div id="app" class=" bg-white">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>--}}
            {{--            </li>--}}

            {{--            <li class="nav-item d-none d-sm-inline-block">--}}
            {{--                <a href="../../index3.html" class="nav-link">Home</a>--}}
            {{--            </li>--}}
            {{--            <li class="nav-item d-none d-sm-inline-block">--}}
            {{--                <a href="#" class="nav-link">Contact</a>--}}
            {{--            </li>--}}
        </ul>


        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
&nbsp;&nbsp;
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">@lang('app.login')</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">@lang('app.registration')</a>
                    </li>
                @endif
            @else
                @php $locale = session()->get('locale');  @endphp
                <a class="btn @if($locale == 'en')btn-primary @else btn-default @endif"  href="/lang/en">En</a>
                <a class="btn  @if($locale == 'ru' || $locale == '')btn-primary @else btn-default @endif" href="/lang/ru">Рус</a>
                <a class="btn  @if($locale == 'kz')btn-primary @else btn-default @endif" href="/lang/kz">Қаз</a> &nbsp;
                <a class="btn btn-info btn-sm" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    @lang('app.logout')
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="position: fixed !important;">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <span class="brand-text font-weight-light"><img src="../../dist/img/brand-inverse.png" alt="User Image" width="100%"></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar" >
            <!-- Sidebar user (optional) -->
            <div class="user-panel">
                <div cl ass="pull-left image" style="float: left!important;">
                    <img src="../../dist/img/empty-account-photo.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <a href="#">
                        @auth
                            {{ Auth::user()->surname }} {{ Auth::user()->name }}
                        @endauth
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    @auth
                        @if(in_array(auth()->user()->role_code, ['operator', 'director_r', 'expert']))
                            <li class="nav-item">
                                <a  href="/home" class="nav-link">
                                    <i class="menu-icon fas fa-home"></i>
                                    <p>
                                        @lang('app.home')
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a  href="{{ route('users') }}" class="nav-link">
                                    <i class="fa fa-fw fa-address-card"></i>
                                    <p>
                                        @lang('app.users')
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a  href="{{ route('centers') }}" class="nav-link">
                                    <i class="menu-icon fas fa-hotel"></i>
                                    <p>
                                        @lang('app.rtc')
                                    </p>
                                </a>
                            </li>
                        @endif
                        @if(!in_array(auth()->user()->role_code, ['student']))
                            <li class="nav-item">
                                @if(!in_array(auth()->user()->role_code, ['teacher', 'student', 'director_s']))
                                    <a  href="{{ route('reports.all') }}" class="nav-link">
                                        @else
                                            <a  href="{{ route('reports.all_') }}" class="nav-link">
                                                @endif
                                                <i class="fa fa-fw fa-file-invoice"></i>
                                                <p>
                                                    @lang('app.reports')
                                                </p>
                                            </a>
                            </li>
                        @endif
                        @if(!in_array(auth()->user()->role_code, ['student']))
                            <li class="nav-item">
                                <a  href="{{ route('orders') }}" class="nav-link">
                                    <i class="menu-icon fas fa-shopping-cart"></i>
                                    <p>
                                        @lang('app.orders')
                                    </p>
                                </a>
                            </li>
                        @endif

                        @if(!in_array(auth()->user()->role_code, ['student', 'developer']))
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="fa fa-wrench"></i>
                                    <p>
                                        @lang('app.references')
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @if(!in_array(auth()->user()->role_code, ['teacher', 'student', 'director_s']))
                                        <li class="nav-item">
                                            <a href="{{ route('institutions') }}" class="nav-link">
                                                <i class="fa fa-fw fa-university"></i>
                                                <p>@lang('app.institutions')</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('schools') }}" class="nav-link">
                                                <i class="fa fa-fw fa-school"></i>
                                                <p>@lang('app.schools')</p>
                                            </a>
                                        </li>
                                    @endif
                                    <li class="nav-item">
                                        <a href="{{ route('groups') }}" class="nav-link">
                                            <i class="fa fa-fw fa-users"></i>
                                            <p>@lang('app.classes')</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="fa fa-fw fa-question-circle"></i>
                                <p>
                                    @lang('app.tests')
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @if(!in_array(auth()->user()->role_code, ['student']))
                                    <li class="nav-item">
                                        <a href="{{ route('subjects') }}" class="nav-link">
                                            <i class="fa fa-fw fa-book"></i>
                                            <p>@lang('app.subjects')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('Categories') }}" class="nav-link">
                                            <i class="fa fa-fw fa-book-open"></i>
                                            <p>@lang('app.topics')</p>
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a href="{{ route('tests') }}" class="nav-link">
                                        <i class="fa fa-fw fa-question-circle"></i>
                                        <p>@lang('app.tests')</p>
                                    </a>
                                </li>
                                @endauth
                            </ul>
                        </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper bg-white">
        <!-- Content Header (Page header) -->
    {{--        <section class="content-header">--}}
    {{--            <div class="container-fluid">--}}
    {{--                <div class="row mb-2">--}}
    {{--                    <div class="col-sm-6">--}}
    {{--                        <h1>Simple Tables</h1>--}}
    {{--                    </div>--}}
    {{--                    <div class="col-sm-6">--}}

    {{--                        <ol class="breadcrumb float-sm-right">--}}
    {{--                            <li class="breadcrumb-item"><a href="#">Home</a></li>--}}
    {{--                            <li class="breadcrumb-item active">Simple Tables</li>--}}
    {{--                        </ol>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div><!-- /.container-fluid -->--}}
    {{--        </section>--}}

    <!-- Main content -->
        <section class="content bg-white">
            <div class="container-fluid ">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        </section>
    </div>
    {{--    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">--}}
    {{--        <div class="container">--}}
    {{--            <a class="navbar-brand" href="{{ url('/') }}">--}}
    {{--                {{ config('app.name', 'Tester') }}--}}
    {{--            </a>--}}
    {{--            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
    {{--                <span class="navbar-toggler-icon"></span>--}}
    {{--            </button>--}}

    {{--            <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
    {{--                <!-- Left Side Of Navbar -->--}}
    {{--                <ul class="navbar-nav mr-auto">--}}
    {{--                    @auth--}}
    {{--                        @if(in_array(auth()->user()->role_code, ['operator', 'director_r', 'expert']))--}}
    {{--                            <li class="nav-item">--}}
    {{--                                <a class="nav-link" href="{{ route('users') }}">Пользователи</a>--}}
    {{--                            </li>--}}
    {{--                            <li class="nav-item">--}}
    {{--                                <a class="nav-link" href="{{ route('centers') }}">РЦТ</a>--}}
    {{--                            </li>--}}
    {{--                        @endif--}}
    {{--                        @if(!in_array(auth()->user()->role_code, ['student']))--}}
    {{--                            <li class="nav-item">--}}
    {{--                                <a class="nav-link" href="{{ route('orders') }}">Заказы</a>--}}
    {{--                            </li>--}}
    {{--                        @endif--}}
    {{--                        @if(!in_array(auth()->user()->role_code, ['student', 'developer']))--}}
    {{--                            <li class="nav-item dropdown">--}}
    {{--                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
    {{--                                    Школы--}}
    {{--                                </a>--}}

    {{--                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
    {{--                                    @if(!in_array(auth()->user()->role_code, ['teacher']))--}}
    {{--                                        <a class="dropdown-item" href="{{ route('schools') }}">Школы</a>--}}
    {{--                                    @endif--}}
    {{--                                    <a class="dropdown-item" href="{{ route('groups') }}">Классы</a>--}}
    {{--                                </div>--}}
    {{--                            </li>--}}
    {{--                        @endif--}}
    {{--                        <li class="nav-item dropdown">--}}
    {{--                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
    {{--                                Тесты--}}
    {{--                            </a>--}}

    {{--                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
    {{--                                @if(!in_array(auth()->user()->role_code, ['student']))--}}
    {{--                                    <a class="dropdown-item" href="{{ route('subjects') }}">Предметы</a>--}}
    {{--                                    <a class="dropdown-item" href="{{ route('Categories') }}">Темы</a>--}}
    {{--                                @endif--}}
    {{--                                <a class="dropdown-item" href="{{ route('tests') }}">Тесты</a>--}}
    {{--                            </div>--}}
    {{--                        </li>--}}
    {{--                    @endauth--}}
    {{--                </ul>--}}

    {{--                <!-- Right Side Of Navbar -->--}}
    {{--                <ul class="navbar-nav ml-auto">--}}
    {{--                    <!-- Authentication Links -->--}}
    {{--                    @guest--}}
    {{--                        <li class="nav-item">--}}
    {{--                            <a class="nav-link" href="{{ route('login') }}">Логин</a>--}}
    {{--                        </li>--}}
    {{--                        @if (Route::has('register'))--}}
    {{--                            <li class="nav-item">--}}
    {{--                                <a class="nav-link" href="{{ route('register') }}">Регистрация</a>--}}
    {{--                            </li>--}}
    {{--                        @endif--}}
    {{--                    @else--}}
    {{--                        <li class="nav-item dropdown">--}}
    {{--                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
    {{--                                {{ Auth::user()->name }} <span class="caret"></span>--}}
    {{--                            </a>--}}

    {{--                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
    {{--                                <a class="dropdown-item" href="{{ route('logout') }}"--}}
    {{--                                   onclick="event.preventDefault();--}}
    {{--                                                     document.getElementById('logout-form').submit();">--}}
    {{--                                    Выйти--}}
    {{--                                </a>--}}

    {{--                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
    {{--                                    @csrf--}}
    {{--                                </form>--}}
    {{--                            </div>--}}
    {{--                        </li>--}}
    {{--                    @endguest--}}
    {{--                </ul>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </nav>--}}

</div>
</body>
</html>
