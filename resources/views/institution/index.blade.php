@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container-fliud">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.institutions')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('institutions.create') }}">@lang('app.Add new institution')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>№</th>
                        <th>@lang('app.Name in Kazakh')</th>
                        <th>@lang('app.Name in Russian')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($institutions as $institution)
                        <tr>
                            <td>{{ $institution->id }}</td>
                            <td>{{ $institution->name_ru }}</td>
                            <td>{{ $institution->name_kz }}</td>
                            <td>
                                <form action="{{ route('institutions.destroy',$institution->id) }}" method="POST">


                                    <a class="btn btn-info btn-sm" href="{{ route('institutions.edit',$institution->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $institutions->links() !!}
            </div>
        </div>
    </div>
@endsection
