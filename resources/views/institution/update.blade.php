@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.Data change') {{$institution->name_kz}}  </h2>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('institutions.update', $institution->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Name in Kazakh'):</strong>
                                <input type="text" name="name_kz" value="{{$institution->name_kz}}" class="form-control" placeholder="@lang('app.Name in Kazakh')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Name in Russian'):</strong>
                                <input type="text" name="name_ru"  value="{{$institution->name_ru}}" class="form-control" placeholder="@lang('app.Name in Russian')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-info btn-sm" href="{{ route('institutions') }}">Назад</a>
                            <button type="submit" class="btn btn-info btn-sm">Сохранить</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
