@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.Regional Testing Centers')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('centers.create') }}">@lang('app.add rtc')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.name_')</th>
                        <th>@lang('app.address')</th>
                        <th>@lang('app.phone')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($centers as $center)
                        <tr>
                            <td>{{ $center->id }}</td>
                            <td>{{ $center->name }}</td>
                            <td>{{ $center->address }}</td>
                            <td>{{ $center->phone }}</td>
                            <td>
                                <form action="{{ route('centers.destroy',$center->id) }}" method="POST">


                                    <a class="btn btn-info btn-sm" href="{{ route('centers.edit',$center->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-danger">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $centers->links() !!}
            </div>
        </div>
    </div>
@endsection
