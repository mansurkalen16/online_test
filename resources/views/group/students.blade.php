@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>{{$group->name}} @lang('app.class')</h2>
                        </div>

                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>@lang('app.IIN')</th>
                        <th>@lang('app.name')</th>
                        <th>@lang('app.surname')</th>
                        <th>@lang('app.email')</th>
                    </tr>
                    @foreach ($group->students() as $student)
                        <tr>
                            <td>{{ $student->iin }}</td>
                            <td>{{ $student->name }}</td>
                            <td>{{ $student->surname }}</td>
                            <td>{{ $student->email }}</td>
                        </tr>
                    @endforeach
                </table>
                <div class="pull-right">
                      <a class="btn btn-info btn-sm" href="{{ route('groups') }}"> @lang('app.Back')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
