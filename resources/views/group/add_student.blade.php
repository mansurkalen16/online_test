@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.add student')</h2>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('groups.save_student')}}" method = "post">

                    @csrf

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.IIN'):</strong>
                                <input type="number" name="iin" class="form-control" placeholder="ИИН">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.name'):</strong>
                                <input type="text" name="name" class="form-control" placeholder="Имя">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.middle name'):</strong>
                                <input type="text" name="middle" class="form-control" placeholder="Отчество">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.surname'):</strong>
                                <input type="text" name="surname" class="form-control" placeholder="Фамилия">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.email'):</strong>
                                <input type="text" name="email" class="form-control" placeholder="Почта">
                            </div>
                        </div>
                        <input type="hidden" name="group_id" value="{{$id}}">
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                           <a class="btn btn-info btn-sm" href="{{ route('groups') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
