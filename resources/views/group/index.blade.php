@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.classes')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('groups.create') }}">@lang('app.add new class')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.school')</th>
                        <th>@lang('app.class')</th>
                        <th>@lang('app.Students_')</th>
                        <th width="660px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($groups as $group)
                        <tr>
                            <td>{{ $group->id }}</td>
                            <td>{{ $group->school()->name }}</td>
                            <td>{{ $group->name }}</td>
                            <td>
                                {{ count($group->students()) }}
                                @if(count($group->students())>0)
                                    <a   href="{{ action('GroupsController@students', ['id' => $group->id]) }}" class="btn btn-info btn-sm"> @lang('app.List') </a>
                                    @endif
                            </td>
                            <td>

                                <form action="{{ route('groups.destroy',$group->id) }}" method="POST">

                                    <a class="btn btn-info btn-sm text-white" onclick="import_('{{$group->id}}')" >@lang('app.import from excel')</a>
                                    <a class="btn btn-info btn-sm" href="{{ route('groups.add_student',$group->id) }}">@lang('app.add student')</a>
                                    <a class="btn btn-info btn-sm" href="{{ route('groups.edit',$group->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-danger">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $groups->links() !!}
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" id="import_" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('app.import from excel')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('users.import')}}" method = "post" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body">
                        <b>@lang('app.import excel file'):</b>
                        <input type="file" name="file" class="form-control" >
                    </div>
                    <input type="hidden" id="class_id" name="class_id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">@lang('app.close')</button>
                        <button type="submit" class="btn btn-info btn-sm" >@lang('app.import')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function import_(id) {
            $('#import_').modal('show')
            document.getElementById("class_id").value = id;
        }
    </script>
@endsection
