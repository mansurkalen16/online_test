@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.Add new user')</h2>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('users.store')}}" method = "post">

                    @csrf

                    <div class="row box box-primary">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.name'):</strong>
                                <input type="text" name="name" class="input-group margin" placeholder="@lang('app.name')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.surname'):</strong>
                                <input type="text" name="middle" class="input-group margin" placeholder="@lang('app.surname')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.middle name'):</strong>
                                <input type="text" name="surname" class="input-group margin" placeholder="@lang('app.middle name')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.email'):</strong>
                                <input type="text" name="email" class="input-group margin" placeholder="@lang('app.email')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.role'):</strong>
                                <select name="role_code" class="form-control">
                                    <option value="operator">Оператор</option>
                                    <option value="director_r">Директор учебного центра</option>
                                    <option value="expert">Эксперт</option>
                                    <option value="developer">Разработчик</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                           <a class="btn btn-info btn-sm" href="{{ route('users') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
