@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.Change user data'): <b>{{$user->surname}} {{$user->name}}</b> </h2>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('users.update',$user->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.name'):</strong>
                                <input type="text" name="name" value="{{$user->name}}" class="form-control" placeholder="@lang('app.name')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.middle name'):</strong>
                                <input type="text" name="middle"  value="{{$user->middle}} "class="form-control" placeholder="@lang('app.middle name')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.surname'):</strong>
                                <input type="text" name="surname" value="{{$user->surname}}" class="form-control" placeholder="@lang('app.surname')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.email'):</strong>
                                <input type="text" name="email"  value="{{$user->email}}" class="form-control" placeholder="@lang('app.email')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Роль:</strong>
                                <select name="role_code" class="form-control">
                                    <option value="operator" @if($user->role_code == "operator") selected @endif>Оператор</option>
                                    <option value="director_r" @if($user->role_code == "director_r") selected @endif>Директор учебного центра</option>
                                    <option value="expert" @if($user->role_code == "expert") selected @endif>Эксперт</option>
                                    <option value="developer" @if($user->role_code == "developer") selected @endif>Разработчик</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                           <a class="btn btn-info btn-sm" href="{{ route('users') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
