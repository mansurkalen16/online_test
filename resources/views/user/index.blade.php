@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.a list of users')</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="{{ route('users.create') }}">@lang('app.Add user')</a>
                        </div><br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.name')</th>
                        <th>@lang('app.surname')</th>
                        <th>@lang('app.role')</th>
                        <th width="280px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->surname }}</td>
                            <td>{{ $user->role_code }}</td>
                            <td>
                                <form action="{{ route('users.destroy',$user->id) }}" method="POST">


                                    <a class="btn btn-info btn-sm" href="{{ route('users.edit',$user->id) }}">@lang('app.Edit')</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-danger">@lang('app.Delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $users->links() !!}
            </div>
        </div>
    </div>
@endsection
