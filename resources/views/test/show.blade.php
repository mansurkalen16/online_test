@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h4>{{$test->name}}</h4>
                <div class="row">
                    @foreach($test->questions() as $question)
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="row">
                                <a href="{{ route('questions.show', $question->id) }}" >{{$question->question}}</a>
                                <form action="{{ route('questions.destroy', $question->id) }}" method="POST">
                                    <a class="btn btn-success btn-sm text-white" onclick="copy('{{$question->id}}')">@lang('app.copy question')</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger">@lang('app.Delete')</button>
                                </form>
                                @foreach($question->answers() as $answer)
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        @if($question->type == 1)
                                            <input type="radio" onchange="" id="answers-{{$answer->id}}"name="answers-{{$question->id}}"> <label for="answers-{{$answer->id}}">{{$answer->answer}}</label>
                                        @else
                                            <input type="checkbox" onchange="" id="answers-{{$answer->id}}"name="answers-{{$question->id}}"> <label for="answers-{{$answer->id}}">{{$answer->answer}}</label>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="copy_" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('app.copy question')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('questions.copy')}}" method = "post">
                    @csrf
                    <div class="modal-body">
                        <b>@lang('app.test name'):</b>
                        <select name="test_id" class="form-control">
                            @foreach($tests as $test)
                                <option value="{{$test->id}}">{{$test->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" id="question_id" name="question_id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">@lang('app.close')</button>
                        <button type="submit" class="btn btn-info btn-sm" >@lang('app.copy question')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function copy(id) {
            $('#copy_').modal('show')
            document.getElementById("question_id").value = id;
        }
    </script>
@endsection
