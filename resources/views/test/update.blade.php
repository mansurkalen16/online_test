@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.Data change'): {{$test->name}}  </h2>
                </div>

                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('tests') }}">@lang('app.Back')</a>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('tests.update', $test->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.name_'):</strong>
                                <input type="text" name="name" value="{{$test->name}}" class="form-control" placeholder="@lang('app.name_')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.Language'):</strong>
                                <select name="language_id" class="form-control">
                                    <option value="1" @if($test->language_id == "1") selected @endif>Қазақша</option>
                                    <option value="2" @if($test->language_id == "2") selected @endif>Русский</option>
                                    <option value="3" @if($test->language_id == "3") selected @endif>English</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.topic'):</strong>
                                <select name="category_id" class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"  @if($category->id == $test->category_id) selected @endif>{{$category->name_kz}} / {{$category->name_ru}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
