@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('app.tests')</h2>
                        </div>
                        @if(in_array(auth()->user()->role_code, ['expert', 'developer']))
                            <div class="pull-right">
                                <a class="btn btn-info btn-sm" href="{{ route('tests.create') }}">@lang('app.add test')</a>
                            </div>
                        @endif
                        <br>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>@lang('app.name_')</th>
                        <th>@lang('app.topic')</th>
                        <th>@lang('app.Number of questions')</th>
                        <th width="360px">@lang('app.actions')</th>
                    </tr>
                    @foreach ($tests as $test)
                        <tr>
                            <td>{{ $test->id }}</td>
                            <td>{{ $test->name }}</td>
                            <td>{{ $test->category()->name_kz }}</td>
                            <td>
                                {{ count($test->questions()) }}
                                @if(in_array(auth()->user()->role_code, ['expert', 'developer']))
                                    <a href="{{ route('tests.show',$test->id) }}">@lang('app.List')</a>
                                @endif
                            </td>
                            <td>
                                @if(in_array(auth()->user()->role_code, ['director_r','director_s', 'teacher', 'operator']))
                                    <a class="btn btn-info btn-sm" href="{{ route('tests.buy', $test->id) }}">@lang('app.Buy test')</a>
                                @endif
                                @if(in_array(auth()->user()->role_code, ['student']))
                                    @if($test->order_for_me()->status_id == 5)
                                        <a class="btn btn-primary btn-sm" href="{{ route('tests.result', $test->id) }}">@lang('app.result')</a>
                                    @elseif($test->order_for_me()->status_id == 1)
                                        <a class="btn btn-info btn-sm" href="{{ route('tests.start', $test->id) }}">@lang('app.start')</a>
                                    @endif
                                @endif
                                @if(in_array(auth()->user()->role_code, ['expert', 'developer']))

                                    <form action="{{ route('tests.destroy',$test->id) }}" method="POST">
                                        <a class="btn btn-success btn-sm" href="{{ route('questions.create', $test->id) }}">@lang('app.add question')</a>
                                        <a class="btn btn-info btn-sm" href="{{ route('tests.edit',$test->id) }}">@lang('app.Edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">@lang('app.Delete')</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $tests->links() !!}
            </div>
        </div>
    </div>
@endsection
