@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td>
                            @lang('app.Handed over'):
                        </td>
                        <td>
                            {{auth()->user()->surname}}
                            {{auth()->user()->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @lang('app.institution'):
                        </td>
                        <td>
                            {{auth()->user()->school()->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @lang('app.test name')
                        </td>
                        <td>
                            {{$test->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @lang('app.result')
                        </td>
                        <td>
                            {{$order->total}}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h5>@lang('app.Right answers')</h5>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h5>@lang('app.answers')</h5>
                    </div>
                    @foreach($test->questions() as $question)
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="row">
                                <a href="{{ route('questions.show', $question->id) }}" >{{$question->question}}</a>
                                @foreach($question->answers() as $answer)
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <p @if($answer->is_correct) class="text-success font-weight-bold" @endif> {{$answer->answer}}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="row">
                                <a href="{{ route('questions.show', $question->id) }}" >{{$question->question}}</a>
                                @foreach($question->answers() as $answer)
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                            <p @if(isset($answer->question()->submitted()->checked) && ($answer->id == $answer->question()->submitted()->checked || in_array($answer->id, explode(',',$answer->question()->submitted()->checked)))) class="text-success font-weight-bold" @endif> {{$answer->answer}}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
