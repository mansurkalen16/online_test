@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4>{{$test->name}}</h4>
                <div class="row">
                    @foreach($test->questionsR() as $question)
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="row">
                                <a href="{{ route('questions.show', $question->id) }}" >{{$question->question}} </a> <b>&nbsp;@if(count($question->answers2())>1) Выберите {{count($question->answers2())}} ответа @endif</b>
                            @foreach($question->answersR() as $answer)
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        @if($question->type == 1)
                                            <input type="radio" onchange="set_answer('1', '{{$question->id}}', '{{$answer->id}}')" id="answers-{{$answer->id}}" name="answers-{{$question->id}}[]"> <label for="answers-{{$answer->id}}">{{$answer->answer}} </label>
                                        @else
                                            <input type="checkbox" onchange="set_answer('2', '{{$question->id}}', '{{$answer->id}}', '{{count($question->answers2())}}')" value="{{$answer->id}}" id="answers-{{$answer->id}}" name="answers-{{$question->id}}[]"> <label for="answers-{{$answer->id}}">{{$answer->answer}}</label>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    <a href="{{route('tests.submit', $test->id)}}" class="btn btn-info btn-sm">@lang('app.finish')</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        function set_answer(type, q_id, a_id, c = null ) {
            let a = '';
            const n = 'answers-'+q_id+'[]';
            const s = document.querySelectorAll('input[name="'+n+'"]:checked').length;
            if(type == '2'){
                if(s <= c) {
                    var inputElements = document.getElementsByName(n);
                    for (var i = 0; inputElements[i]; ++i) {
                        if (inputElements[i].checked) {
                            if (s == 1) {
                                a = inputElements[i].value;
                                break;
                            } else {
                                a = a + "," + inputElements[i].value;
                            }
                        }
                    }
                }else{
                    $('#answers-'+a_id).prop('checked', false);
                }
            }else{
                a=a_id;
            }
            if(a[0] == ','){
                a = a.substr(1)
            }
            if(a.length>1){
                $.post("/tests/answer",
                    {
                        "_token": "{{ csrf_token() }}",
                        "type": type,
                        "a": a,
                        "q": q_id,

                    },
                    function(data, status){
                        console.log("Data: " + data + "\nStatus: " + status);
                    });
            }
        }
    </script>
@endsection
