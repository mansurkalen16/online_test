@extends('layouts.app')

@section('content')
    <style>
        .hidden{
            display: none !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h4>@lang('app.test name'): {{$test->name}}</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        @if(in_array(auth()->user()->role_code, ['teacher', 'director_s']))
                            <form action="{{ route('tests.buy_') }}" method="POST">
                                @csrf
                                <div class="panel-group" id="accordion">
                                    @foreach($groups as $group)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$group->id}}">
                                                        {{$group->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse{{$group->id}}" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    @foreach($group->students() as $student)
                                                        <input type="checkbox" name="student_id[]" id="{{$student->id}}" value="{{$student->id}}" />
                                                        <label for="{{$student->id}}">{{$student->surname}} {{$student->name}}</label><br>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <input type="hidden" name="test_id" value="{{$test->id}}">
                                <input type="hidden" name="school_id" value="{{auth()->user()->school_id}}">
                                <button type="submit" class="btn btn-success">@lang('app.Buy')</button>
                            </form>
                        @endif
                        @if(in_array(auth()->user()->role_code, ['operator', 'director_r']))
                            <form action="{{ route('tests.buy_') }}" method="POST">
                                @csrf
                                <select name="school_id" class="form-control" onchange="show(this)" >
                                    <option  value="0">Выберите школу</option>
                                    @foreach($schools as $school)
                                        <option  value="{{$school->id}}">{{$school->name}}</option>
                                    @endforeach
                                </select><br>
                                @foreach($schools as $school)
                                    <div class="panel-group schools {{$school->id}} hidden" id="accordion">
                                        {{$school->name}}
                                        @foreach($school->groups() as $group)
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$group->id}}">
                                                            {{$group->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse{{$group->id}}" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        @foreach($group->students() as $student)
                                                            <input type="checkbox" name="student_id[]" id="{{$student->id}}" value="{{$student->id}}" />
                                                            <label for="{{$student->id}}">{{$student->surname}} {{$student->name}}</label><br>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                    <br>
                                    <input type="hidden" name="test_id" value="{{$test->id}}">
                                    <button type="submit" class="btn btn-success btn-sm">@lang('app.Buy')</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function show(data) {
            $('.schools').addClass('hidden');
            $('.'+data.value).removeClass('hidden');
        }
    </script>
@endsection
