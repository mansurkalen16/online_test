@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>@lang('app.add question')</h2>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('questions.store')}}" method = "post">

                    @csrf

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.question'):</strong>
                                <input type="text" name="question" class="form-control" placeholder="Вопрос">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.number of answer'):</strong>
                                <input type="number" name="answer_count" class="form-control" placeholder="@lang('app.number of answer')">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.level'):</strong>
                                <select name="level" class="form-control">
                                    <option value="1">Легкий</option>
                                    <option value="2">Средний</option>
                                    <option value="3">Сложный</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@lang('app.number of correct answer'):</strong>
                                <select name="type" class="form-control">
                                    <option value="1">Один ответ</option>
                                    <option value="2">Несколько ответов</option>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="test_id" value="{{$test_id}}">
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-info btn-sm" href="{{ route('tests') }}">@lang('app.Back')</a>
                            <button type="submit" class="btn btn-info btn-sm">@lang('app.Save')</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
