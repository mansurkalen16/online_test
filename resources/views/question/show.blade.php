@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" >
    <script type="text/javascript" src="{{ asset('/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.js')}}" ></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <b >{{$question->question}}</b> <a href="{{ route('questions.edit', $question->id) }}" >@lang('app.Edit')</a>
                @foreach($question->answers() as $answer)
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        @if($question->type == 1)
                            <input type="radio" onchange="" id="answers-{{$answer->id}}" name="answers-{{$question->id}}"> <label for="answers-{{$answer->id}}">{{$answer->answer}}</label>
                        @else
                            <input type="checkbox" onchange="" id="answers-{{$answer->id}}" name="answers-{{$question->id}}"> <label for="answers-{{$answer->id}}">{{$answer->answer}}</label>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
