<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('lang/{locale}', 'LocalizationController@index');
Route::group(['middleware' => ['auth']], function() {

    Route::get('/orders/create', 'OrdersController@create')->name('orders.create');
    Route::get('/orders', 'OrdersController@index')->name('orders');
    Route::get('/orders/{id}', 'OrdersController@show')->name('orders.show');
    Route::get('/orders/{id}/edit', 'OrdersController@edit')->name('orders.edit');
    Route::put('/orders/{id}', 'OrdersController@update')->name('orders.update');
    Route::post('/orders', 'OrdersController@store')->name('orders.store');
    Route::delete('/orders/{id}', 'OrdersController@destroy')->name('orders.destroy');


    Route::get('/reports/all', 'ReportsController@all')->name('reports.all');
    Route::get('/reports/all_', 'ReportsController@all_')->name('reports.all_');
    Route::get('/reports/{id}', 'ReportsController@index')->name('reports');
    Route::get('/reports/{id}/center/{tid}', 'ReportsController@center')->name('reports.center');
    Route::get('/reports/{id}/school/{tid}', 'ReportsController@school')->name('reports.school');
    Route::get('/reports/{id}/group/{tid}', 'ReportsController@group')->name('reports.group');
    Route::get('/reports/{id}/student/{tid}', 'ReportsController@student')->name('reports.student');

    Route::get('/institutions/create', 'InstitutionsController@create')->name('institutions.create');
    Route::get('/institutions', 'InstitutionsController@index')->name('institutions');
    Route::get('/institutions/{id}', 'InstitutionsController@show')->name('institutions.show');
    Route::get('/institutions/{id}/edit', 'InstitutionsController@edit')->name('institutions.edit');
    Route::put('/institutions/{id}', 'InstitutionsController@update')->name('institutions.update');
    Route::post('/institutions', 'InstitutionsController@store')->name('institutions.store');
    Route::delete('/institutions/{id}', 'InstitutionsController@destroy')->name('institutions.destroy');

    Route::get('/centers/create', 'CentersController@create')->name('centers.create');
    Route::get('/centers', 'CentersController@index')->name('centers');
    Route::get('/centers/{id}', 'CentersController@show')->name('centers.show');
    Route::get('/centers/{id}/edit', 'CentersController@edit')->name('centers.edit');
    Route::put('/centers/{id}', 'CentersController@update')->name('centers.update');
    Route::post('/centers', 'CentersController@store')->name('centers.store');
    Route::delete('/centers/{id}', 'CentersController@destroy')->name('centers.destroy');

    Route::get('/schools/create', 'SchoolsController@create')->name('schools.create');
    Route::get('/schools', 'SchoolsController@index')->name('schools');
    Route::get('/schools/{id}', 'SchoolsController@show')->name('schools.show');
    Route::get('/schools/{id}/edit', 'SchoolsController@edit')->name('schools.edit');
    Route::put('/schools/{id}', 'SchoolsController@update')->name('schools.update');
    Route::post('/schools', 'SchoolsController@store')->name('schools.store');
    Route::delete('/schools/{id}', 'SchoolsController@destroy')->name('schools.destroy');
    Route::post('/schools/import', 'SchoolsController@import')->name('schools.import');

    Route::get('/groups/create', 'GroupsController@create')->name('groups.create');
    Route::get('/groups', 'GroupsController@index')->name('groups');
    Route::get('/groups/{id}', 'GroupsController@show')->name('groups.show');
    Route::get('/groups/{id}/edit', 'GroupsController@edit')->name('groups.edit');
    Route::put('/groups/{id}', 'GroupsController@update')->name('groups.update');
    Route::post('/groups', 'GroupsController@store')->name('groups.store');
    Route::delete('/groups/{id}', 'GroupsController@destroy')->name('groups.destroy');
    Route::get('/groups/add_student/{id}', 'GroupsController@add_student')->name('groups.add_student');
    Route::post('/groups/save', 'GroupsController@save_student')->name('groups.save_student');
    Route::get('/students', 'GroupsController@students')->name('students');

    Route::get('/users/create', 'UsersController@create')->name('users.create');
    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/users/{id}', 'UsersController@show')->name('users.show');
    Route::get('/users/{id}/edit', 'UsersController@edit')->name('users.edit');
    Route::put('/users/{id}', 'UsersController@update')->name('users.update');
    Route::post('/users', 'UsersController@store')->name('users.store');
    Route::post('/users/import', 'UsersController@import')->name('users.import');
    Route::delete('/users/{id}', 'UsersController@destroy')->name('users.destroy');

    Route::get('/subjects/create', 'SubjectsController@create')->name('subjects.create');
    Route::get('/subjects', 'SubjectsController@index')->name('subjects');
    Route::get('/subjects/{id}', 'SubjectsController@show')->name('subjects.show');
    Route::get('/subjects/{id}/edit', 'SubjectsController@edit')->name('subjects.edit');
    Route::put('/subjects/{id}', 'SubjectsController@update')->name('subjects.update');
    Route::post('/subjects', 'SubjectsController@store')->name('subjects.store');
    Route::delete('/subjects/{id}', 'SubjectsController@destroy')->name('subjects.destroy');

    Route::get('/Categories/create', 'CategoriesController@create')->name('Categories.create');
    Route::get('/Categories', 'CategoriesController@index')->name('Categories');
    Route::get('/Categories/{id}', 'CategoriesController@show')->name('Categories.show');
    Route::get('/Categories/{id}/edit', 'CategoriesController@edit')->name('Categories.edit');
    Route::put('/Categories/{id}', 'CategoriesController@update')->name('Categories.update');
    Route::post('/Categories', 'CategoriesController@store')->name('Categories.store');
    Route::delete('/Categories/{id}', 'CategoriesController@destroy')->name('Categories.destroy');

    Route::get('/tests/create', 'TestsController@create')->name('tests.create');
    Route::get('/tests', 'TestsController@index')->name('tests');
    Route::get('/tests/{id}', 'TestsController@show')->name('tests.show');
    Route::get('/tests/{id}/start', 'TestsController@start')->name('tests.start');
    Route::post('/tests/answer', 'TestsController@answer')->name('tests.answer');
    Route::get('/tests/{id}/submit', 'TestsController@submit')->name('tests.submit');
    Route::get('/tests/{id}/result', 'TestsController@result')->name('tests.result');
    Route::get('/tests/{id}/buy', 'TestsController@buy')->name('tests.buy');
    Route::post('/tests/buy', 'TestsController@buy_')->name('tests.buy_');
    Route::get('/tests/{id}/edit', 'TestsController@edit')->name('tests.edit');
    Route::put('/tests/{id}', 'TestsController@update')->name('tests.update');
    Route::post('/tests', 'TestsController@store')->name('tests.store');
    Route::delete('/tests/{id}', 'TestsController@destroy')->name('tests.destroy');

    Route::get('/questions/create/{id}', 'QuestionsController@create')->name('questions.create');
    Route::get('/questions', 'QuestionsController@index')->name('questions');
    Route::get('/questions/{id}', 'QuestionsController@show')->name('questions.show');
    Route::get('/questions/{id}/edit', 'QuestionsController@edit')->name('questions.edit');
    Route::put('/questions/{id}', 'QuestionsController@update')->name('questions.update');
    Route::post('/questions_copy', 'QuestionsController@copy')->name('questions.copy');
    Route::post('/questions', 'QuestionsController@store')->name('questions.store');
    Route::delete('/questions/{id}', 'QuestionsController@destroy')->name('questions.destroy');

    Route::get('/answers/create/{id}', 'AnswersController@create')->name('answers.create');
    Route::get('/answers', 'AnswersController@index')->name('answers');
    Route::get('/answers/{id}', 'AnswersController@show')->name('answers.show');
    Route::get('/answers/{id}/edit', 'AnswersController@edit')->name('answers.edit');
    Route::put('/answers', 'AnswersController@update')->name('answers.update');
    Route::post('/answers', 'AnswersController@store')->name('answers.store');
    Route::delete('/answers/{id}', 'AnswersController@destroy')->name('answers.destroy');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
