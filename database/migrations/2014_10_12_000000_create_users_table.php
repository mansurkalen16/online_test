<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('iin')->unique()->nullable();
            $table->string('name');
            $table->string('middle')->nullable();
            $table->string('surname');
            $table->integer('center_id')->nullable();
            $table->integer('school_id')->nullable();
            $table->integer('class_id')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('role_code');
            $table->bigInteger('author_id');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                'name' => 'dev',
                'surname' => 'deev',
                'email' => 'mansurkalen16@gmail.com',
                'password' => Hash::make('123'),
                'role_code' => 'developer',
                'author_id' => '1'
            )
        );
        DB::table('users')->insert(
            array(
                'name' => 'exp',
                'surname' => 'eexp',
                'email' => 'mansurkalen1@gmail.com',
                'password' => Hash::make('123'),
                'role_code' => 'expert',
                'author_id' => '1'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
