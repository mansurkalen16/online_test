<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('roles')->insert(
        array(
            'code' => 'director_r',
            'name' => 'Директор РЦТ'
        ));

        DB::table('roles')->insert(array(
            'code' => 'developer',
            'name' => 'Разработчик'
        ));

        DB::table('roles')->insert(
        array(
            'code' => 'expert',
            'name' => 'Эксперт'
        ));

        DB::table('roles')->insert(
        array(
            'code' => 'operator',
            'name' => 'Оператор'
        ));

        DB::table('roles')->insert(
        array(
            'code' => 'director_s',
            'name' => 'Директор школы'
        ));

        DB::table('roles')->insert(
        array(
            'code' => 'teacher',
            'name' => 'Учитель'
        ));

        DB::table('roles')->insert(
        array(
            'code' => 'Student',
            'name' => 'Ученик'
        )
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
