<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateLangugagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('short_name');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('languages')->insert(
            array(
                'code' => 'kz',
                'short_name' => 'Қаз',
                'name' => 'Қазақша'
            ),
            array(
                'code' => 'ru',
                'short_name' => 'Рус',
                'name' => 'Русский'
            ),
            array(
                'code' => 'kz-ru',
                'short_name' => 'Қаз-Рус',
                'name' => 'Қазақша-Русский'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('langugages');
    }
}
